package controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import repository.StudentRepository;

@RestController
public class StudentController {

    private final StudentRepository studentRepository;


    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @GetMapping("/student/{group_id}")
    public ResponseEntity<?> getStudentsById(@PathVariable long group_id) {
        return ResponseEntity.ok(studentRepository.findAllByGroupId(group_id));
    }



}
